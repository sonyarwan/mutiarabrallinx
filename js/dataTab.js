$(document).ready(function () {

    gapi.load('client', initClient);
    
    
});

// var element = document.getElementById("myprogressBar");    
// var width = 1; 
// var identity = setInterval(scene, 10); 
// function scene() { 
//     if (width >= 100) { 
//     clearInterval(identity); 
//     } else { 
//     width++;  
//     element.style.width = width + '%';  
//     } 
// } 

function initClient() {
    // $('#myprogressBar').show();
    $('#loadingBar').show();
    $('#table_Data').hide();
gapi.client.init({
    apiKey: 'AIzaSyB-CvzXVaT3jF7loXuqfms0VVTGHeu4D18',
    discoveryDocs: ['https://sheets.googleapis.com/$discovery/rest?version=v4'],
}).then(function() {
    // Pastikan untuk mengganti 'SPREADSHEET_ID' dan 'SHEET_NAME' dengan ID spreadsheet dan nama lembar yang diinginkan
    return gapi.client.sheets.spreadsheets.values.get({
        spreadsheetId: '1rZTjDu1jOAL0w1X_WnTYK4I2maMbpn9vSW85pPk2lOc',
        // DATA YAMG LEBIH SEDIKIT //'1oijzYetyTE6oNkkxP1rNATTN5dnRnV11MjZKEgnTodI',
        // DATA REAL 70.000 AN  1rZTjDu1jOAL0w1X_WnTYK4I2maMbpn9vSW85pPk2lOc
        range: 'Sheet1!A:E', // Ubah rentang sel sesuai kebutuhan
    });
}).then(function(response) {
   console.log('masih loading')
   var values = response.result.values; 
   var dataSource = arrayToJSONObject(values);

 

$('#dtExample').DataTable({
    dom: 'Bfrtip',
    data: dataSource,
    columns: [
        {
            render: function (data, type, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
            }
        },
        { data: 'PARTNUMBER' },
        { data: 'PARTNAME' },
        { data: 'MODEL' }
    ],
    "processing" : true,
    "paging": true,
    "info": true,
    "language": {
        'loadingRecords': '&nbsp;',
        'processing': 'Loading...'
    },
    "fnRowCallback": function (nRow, aData, iDisplayIndex) {
        $("td:first", nRow).html(iDisplayIndex + 1);
        return nRow;
    }
    
});
// $('#myprogressBar').hide();
$('#loadingBar').hide();
$('#table_Data').show();
return arrayToJSONObject(values);  
}, function(error) {
   console.error('Error fetching data from Google Sheets:', error);
});

} 

function arrayToJSONObject(arr) {
var keys = arr[0];

var newArr = arr.slice(1, arr.length);

var formatted = [],
data = newArr,
cols = keys,
l = cols.length;
for (var i = 0; i < data.length; i++) {
var d = data[i],
  o = {};
for (var j = 0; j < l; j++) o[cols[j]] = d[j];
formatted.push(o);
}
return formatted;
}
