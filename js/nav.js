const nav = `<a href="index.html" class="navbar-brand d-flex align-items-center px-2 px-lg-5">
<h2 class="m-0 text-primary"><img src="img/logo-3d.jpg" style="width: 40px; height: 30px;"></i> PT. MUTIARA BRALLINX SEJAHTERA</h2>
</a>
<button type="button" class="navbar-toggler me-4" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
<span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse" id="navbarCollapse">
<div class="navbar-nav ms-auto p-4 p-lg-0">
    <a href="index.html" class="nav-item nav-link">Home</a>
    <a href="about.html" class="nav-item nav-link">About</a>
    <a href="spareparts-list.html" class="nav-item nav-link">List Suku Cadang</a>
    <div class="nav-item dropdown">
        <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">Produk</a>
        <div class="dropdown-menu fade-up m-0">
            <a href="spareparts.html" class="dropdown-item">Suku Cadang</a>
            <a href="repaired.html" class="dropdown-item">Body Repaired</a>
            
        </div>
    </div>
    <a href="contact.html" class="nav-item nav-link">Contact</a>
</div>
</div>
`

const div = `<div class="container" style="padding-top: 10px !important;padding-bottom: 10px !important;">
<div class="row g-5">
    <div class="col-lg-4 col-md-6">
        <h4 class="text-light mb-4">Alamat</h4>
        <p class="mb-2"><i class="fa fa-map-marker-alt me-3"></i>Jl. Perjuangan No 86 RT 001 Rw006 Harapan Baru, Bekasi utara, Kota Bekasi ,Jawa Barat</p>
        <p class="mb-2"><i class="fa fa-envelope me-3"></i>mutiarabrallinxsejahtera@gmail.com</p>
    </div>
    <div class="col-lg-4 col-md-6">
        <h4 class="text-light mb-4">Jam Operasional</h4>
        <h6 class="text-light">Senin - Jumat :</h6>
        <p class="mb-4">09.00 WIB - 21.00 WIB</p>
        <h6 class="text-light">Sabtu - Minggu :</h6>
        <p class="mb-0">09.00 WIB - 12.00 WIB</p>
    </div>
    <div class="col-lg-4 col-md-6">
        <h4 class="text-light mb-4">Hubungi Kami</h4>
        <p class="mb-2"><i class="fa fa-phone-alt me-3"></i>0856 800 7881 | 0813 1000 6042</p>
       
    </div>
</div>
</div>
<div class="container" style="padding-top: 5px;padding-bottom: 5px;">
<div class="copyright">
    <div class="row">
        <div class="col-md-12 text-center">
            Copyright &copy; 2023 - All Right Reserved - PT MUTIARA BRALLINX SEJAHTERA
            
        </div>
        
    </div>
</div>
</div>`

const divListCust = `
    <div class="container" style="padding-top: 10px !important;padding-bottom: 10px !important;">
        <div class="text-center">
            <h1 class="mb-5">Daftar Pelanggan</h1>
        </div>
        <div class="owl-carousel testimonial-carousel position-relative">
            <div class="testimonial-item text-center">
                <img class="bg-light rounded-circle p-2 mx-auto mb-3" src="img/clients/pt_masaji_prayasa_cargo_logo.png" style="width: 80px; height: 80px;">
                <h5 class="mb-0">PT Masaji Prayasa Cargo & Contraktor</h5>
                <p>Company</p>
                <div class="testimonial-text bg-light text-center p-4">
                <p class="mb-1">Jl. Cilincing Raya No. 17, Jakarta</p>
                </div>
            </div>
            <div class="testimonial-item text-center">
                <img class="bg-light rounded-circle p-2 mx-auto mb-3" src="img/clients/abinusa.png" style="width: 80px; height: 80px;">
                <h5 class="mb-0">PT Anugerah Buminusantara Abadi Contractor</h5>
                <p>Company</p>
                <div class="testimonial-text bg-light text-center p-4">
                <p class="mb-1">Jl. Cassablanca No. 18 , Wisma Staco Lt.17 Jakarta.</p>
                </div>
            </div>
            <div class="testimonial-item text-center">
                <img class="bg-light rounded-circle p-2 mx-auto mb-3" src="img/DSP.jpg" style="width: 80px; height: 80px;">
                <h5 class="mb-0">PT Duta Sarana Perkasa Manufacturing</h5>
                <p>Company</p>
                <div class="testimonial-text bg-light text-center p-4">
                <p class="mb-1">Alia Building 5th Floor, Jakarta.</p>
                </div>
            </div>
            <div class="testimonial-item text-center">
                <img class="bg-light rounded-circle p-2 mx-auto mb-3" src="img/clients/pt-lobunta-kencana-raya-3.png" style="width: 80px; height: 80px;">
                <h5 class="mb-0">PT Lobunta Kencana Raya Mining Contractor</h5>
                <p>Company</p>
                <div class="testimonial-text bg-light text-center p-4">
                <p class="mb-1">Jl. Pintu Air No.31B, Jakarta.</p>
                </div>
            </div>
            <div class="testimonial-item text-center">
                <img class="bg-light rounded-circle p-2 mx-auto mb-3" src="img/clients/Great-Giant-Pineapple.png" style="width: 80px; height: 80px;">
                <h5 class="mb-0">PT. GREAT GIANT PINEAPPLE</h5>
                <p>Company</p>
                <div class="testimonial-text bg-light text-center p-4">
                <p class="mb-1">Jl. Jend. Sudirman No.Kav 71, RT.5/RW.3, Jakarta Selatan, 12190.</p>
                </div>
            </div>
        </div>
    </div>`

